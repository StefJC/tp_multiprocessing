from Bio import SeqIO
from multiprocessing import Pool


fasta = SeqIO.parse('some_seq_from_ensembl.fa', 'fasta')
new_fasta = []

for record in fasta:
    new_fasta.append(record.seq)
print(new_fasta)



def count_nucs(sequence):
    dico_nucs = {}
    for sequ in sequence:
        for nucs in range(len(sequ)):
            if sequ[nucs] not in dico_nucs:
                dico_nucs[sequ[nucs]] = 1
            else:
                dico_nucs[sequ[nucs]] += 1
    return dico_nucs

if __name__ == '__main__':
    p = Pool(5)
    print(p.map(count_nucs, new_fasta))
